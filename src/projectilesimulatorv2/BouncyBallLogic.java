package projectilesimulatorv2;

import javafx.animation.AnimationTimer;
import javafx.concurrent.Task;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class BouncyBallLogic extends Logic{
    double lastDisplacement = 0;
    @Override
    public void Run(LineChart graph, XYChart.Series series, double initialVelValue, double angleValue, double gravityValue, int counter){
//        double newMaxHeight = ProjectileCalculations.GetMaxHeight(initialVelValue, angleValue, gravityValue);
//        while(newMaxHeight>1){
//           newMaxHeight = ProjectileCalculations.GetMaxHeight(initialVelValue, angleValue, gravityValue);
           plotPoints(graph, series, initialVelValue, angleValue,gravityValue, counter);
//           initialVelValue = ProjectileCalculations.GetFinalVel(initialVelValue, angleValue, gravityValue)/2;
//       }
       graph.getData().add(series);
    }
    @Override
    public void plotPoints(LineChart graph, XYChart.Series series, double initialVelValue, double angleValue, double gravityValue, int counter){
        final DoubleRecorder totalDisplacement = new DoubleRecorder(0);
        final DoubleRecorder startTime = new DoubleRecorder(0);
        startTime.set(System.nanoTime());
        final DoubleRecorder velValue = new DoubleRecorder(initialVelValue);
        final BooleanRecorder hasBounced = new BooleanRecorder(false);
        
        Task<Void> task = new Task<Void>(){
            @Override
             public Void call() throws Exception{
                new AnimationTimer() {
                @Override
                public void handle(long now) {
                    double elapsedTime = (now - startTime.get())/1000000000d;
                    //double elapsedTime = 0;
                    //updateGraph(graph, velValue, angleValue, gravityValue, 0, ProjectileCalculations.GetTotalDisplacement(0,velValue, angleValue, gravityValue),series, elapsedTime);
                    double vDisplacement = ProjectileCalculations.GetVDisplacement(velValue.get(), angleValue, gravityValue, elapsedTime);
                    System.out.println("vDisplacement" + vDisplacement);
                if(vDisplacement < 0 && !hasBounced.get()) {
                    //this.stop();
                    startTime.set(System.nanoTime());
                    elapsedTime = (now - startTime.get())/1000000000d;
                    lastDisplacement = ProjectileCalculations.GetTotalDisplacement(lastDisplacement,velValue.get(), angleValue, gravityValue);
                    totalDisplacement.set(lastDisplacement);
                    System.out.println("lastDisplacement" + lastDisplacement);
                    velValue.set(ProjectileCalculations.ReduceInitialVel(velValue.get(), 0.5, angleValue));
                    updateGraph(graph, velValue.get(), angleValue, gravityValue, lastDisplacement, ProjectileCalculations.GetTotalDisplacement(lastDisplacement, velValue.get(), angleValue, gravityValue),series, elapsedTime);
                    System.out.println("Reached 0?");
                }
                else if(ProjectileCalculations.GetMaxHeight(velValue.get(), angleValue, gravityValue)<0.5){
                    System.out.println("stopped?" + ProjectileCalculations.GetMaxHeight(velValue.get(), angleValue, gravityValue));
                    lastDisplacement = 0;
                    totalDisplacement.set(0);
                    this.stop();
                }else{
                   elapsedTime = (now - startTime.get())/1000000000d;
                   updateGraph(graph, velValue.get(), angleValue, gravityValue, totalDisplacement.get(), ProjectileCalculations.GetTotalDisplacement(totalDisplacement.get(),velValue.get(), angleValue, gravityValue),series, elapsedTime);
                   System.out.println("Plotting point");
                }   
                if (vDisplacement > 0 && hasBounced.get()) {
                    hasBounced.set(false);
                }
            }
                }.start();
                return null;
            }
        };
        new Thread(task).start();
        
    }
    
    public void setAxis(LineChart graph, double initialVelValue, double angleValue, double gravityValue){
        ((NumberAxis) graph.getYAxis()).setAutoRanging(false); // Allow manual choosing of range
        ((NumberAxis) graph.getYAxis()).setLowerBound(0); // Set start and stop of axis
        ((NumberAxis) graph.getYAxis()).setUpperBound(ProjectileCalculations.GetMaxHeight(initialVelValue, angleValue, gravityValue));
        ((NumberAxis) graph.getXAxis()).setAutoRanging(false); // Allow manual choosing of range
        ((NumberAxis) graph.getXAxis()).setLowerBound(0); // Set start and stop of axis
        ((NumberAxis) graph.getXAxis()).setUpperBound(ProjectileCalculations.GetTotalDisplacement(0, initialVelValue, angleValue, gravityValue, 0.5)); 
    }
    
}
