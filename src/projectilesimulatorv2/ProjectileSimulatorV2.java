package projectilesimulatorv2;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
public class ProjectileSimulatorV2 extends Application {
Stage window;   
Scene scene;

Button projectileButton=new Button("USE PROJECTILE");
Button bouncyBallButton=new Button("USE BOUNCY BALL");
Label title = new Label("PROJECTILE SIMULATOR");
public static void main(String[] args) {
    launch(args);
}
@Override
public void start(Stage mainMenu)throws Exception{
    window=mainMenu;
    SetTitleOfProgram();
    
    int buttonChoice;

    BorderPane layout = new BorderPane();
    SetBackgroundColour(layout); 
    VBox menu = new VBox(3);    
    SetSpacingBetweenButtons(menu, 20);
    SetButtonsToCentre(menu);

    AddTitleToMenu(menu, title);
    AddButtonToMenu(menu, projectileButton);
    AddButtonToMenu(menu, bouncyBallButton);

    AddStyleToButton(projectileButton); 
    AddStyleToButton(bouncyBallButton);
    AddStyleToLabel(title, "title");

    SetSizeOfButton(projectileButton, 600, 100);
    SetSizeOfButton(bouncyBallButton, 600, 100);

    SetSceneWhenButtonIsPressed(projectileButton, new ProjectileLogic());
    SetSceneWhenButtonIsPressed(bouncyBallButton, new BouncyBallLogic());

    scene = new Scene(layout);
    SetFullscreen();
    layout.setCenter(menu); 

    SetCurrentScene();
    SetDefultStyle("Style.css");
    DisplayWindow();  
}

private void SetCurrentScene() {
    window.setScene(scene);
}

private void DisplayWindow() {
    window.show();
}

private void SetDefultStyle(String styleSheet) {
    window.getScene().getStylesheets().add(getClass().getResource(styleSheet).toExternalForm());
}

private void SetFullscreen() {
    window.setFullScreen(true);
}

private void SetSceneWhenButtonIsPressed(Button button, Logic logic) {
    button.setOnAction(e -> {
        Display display = new Display(logic);
        window.getScene().setRoot(display.getScene());
        SetFullscreen();
        
    });
}

public static void SetSizeOfButton(Button button, int xValue, int yValue) {
    button.setPrefSize(xValue, yValue);
}

public static void AddStyleToLabel(Label label, String style) {
    label.getStyleClass().add(style);
}

public static void AddStyleToButton(Button button) {
    button.getStyleClass().add("button");
}

private void SetTitleOfProgram() {
    window.setTitle("Projectile Simulator");
}

private void AddButtonToMenu(VBox menu, Button button) {
    menu.getChildren().add(button);
}

private void AddTitleToMenu(VBox menu, Label title) {
    menu.getChildren().add(title);
}

private void SetButtonsToCentre(VBox menu) {
    menu.setAlignment(Pos.CENTER);
}

private void SetSpacingBetweenButtons(VBox menu, int spacing) {
    menu.setSpacing(spacing);
}

public static void SetBackgroundColour(BorderPane container) {
    container.setStyle("-fx-background-color: #8dd4fa;");
}


}
