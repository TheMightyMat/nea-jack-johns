package projectilesimulatorv2;

import java.text.DecimalFormat;
import javafx.animation.AnimationTimer;
import javafx.concurrent.Task;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;

public abstract class Logic {
    public abstract void Run(LineChart graph, XYChart.Series series, double initialVelValue, double angleValue, double gravityValue, int counter);
    
    public void buttonAction(LineChart graph, String seriesName, double initialVelValue, double angleValue, double gravityValue, TextField totalDisplacementDisplay, TextField totalTimeDisplay, TextField maxHeightDisplay, TextField finalVelDisplay, int counter){
        setAxis(graph, initialVelValue, angleValue, gravityValue);  
        // Define series
        XYChart.Series series = new XYChart.Series();
        graph.setLegendVisible(false);
        Run(graph, series, initialVelValue, angleValue,gravityValue, counter);
        //Updates the text fields to display the outputs
        DecimalFormat dp = new DecimalFormat("#.00");
        String totalDisplacementString = dp.format(ProjectileCalculations.GetTotalDisplacement(0, initialVelValue, angleValue, gravityValue));
        String totalTimeString = dp.format(ProjectileCalculations.GetTotalTime(initialVelValue, angleValue, gravityValue));
        String maxHeightString = dp.format(ProjectileCalculations.GetMaxHeight(initialVelValue, angleValue, gravityValue));
        String finalVelString = dp.format(ProjectileCalculations.GetFinalVel(initialVelValue, angleValue, gravityValue));
        totalDisplacementDisplay.setText(totalDisplacementString + "m");
        totalTimeDisplay.setText(totalTimeString + "s");
        maxHeightDisplay.setText(maxHeightString + "m");
        finalVelDisplay.setText(finalVelString + "m/s");
    }
    public void updateGraph(LineChart chart, double initialVel, double angle, double gravity, double start, double stop, XYChart.Series series, double elapsedTime){
        double step = (stop-start) / 2000; // Divide the visible area into 2000 parts   
        double vDisplacement = ProjectileCalculations.GetVDisplacement(initialVel, angle, gravity, elapsedTime);
        System.out.println(vDisplacement);
        double hDisplacement = ProjectileCalculations.GetHDisplacement(start,initialVel, angle, elapsedTime);
        System.out.println("HDISPLACEMENT ------ " + hDisplacement);
        boolean newChart = false;
        //if (chart.getData().size() > 0) {
        //    newChart = true;
        //    series = (XYChart.Series) chart.getData().get(0);
        //} else {
        //    series = new Series();
        //}
        //series.getData().clear();
        series.getData().add(new XYChart.Data(hDisplacement, vDisplacement));
    }
    public LineChart getGraph(String chartTitle) {
        // Define the axes
        NumberAxis xAxis = new NumberAxis();
        NumberAxis yAxis = new NumberAxis();
        
        // Create the chart
        LineChart chart = new LineChart(xAxis,yAxis);
        chart.setTitle(chartTitle);     // Title of the graph
        
        chart.setCreateSymbols(false); // Hide dots
        
        xAxis.setTickLabelRotation(90);
        
        return chart;
    }
    public abstract void plotPoints(LineChart graph, XYChart.Series series, double initialVelValue, double angleValue, double gravityValue, int counter);
    
    public abstract void setAxis(LineChart graph, double initialVelValue, double angleValue, double gravityValue);
}

