
package projectilesimulatorv2;

public class DoubleRecorder {
    private double value;
    
    public DoubleRecorder(double value) {
        this.value = value;
    }
    
    public void set(double value) {
        this.value = value;
    }
    
    public double get() {
        return this.value;
    }
}
