package projectilesimulatorv2;
import java.text.DecimalFormat;
import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
public class Display {
    private Logic logic;
    public Display(Logic logic){
        this.logic = logic;
    }
    public BorderPane getScene() {
        String chartTitle = SetSimulationTitle(" ");
        int counter =0;
        //Pre-defines all of the items used
        Label totalDisplacement = new Label("Displacement");
        Label maxHeight = new Label("Maximum Height");
        Label finalVel = new Label("Final Velocity");
        Label totalTime = new Label("Total Time");
        
        TextField totalDisplacementDisplay = new TextField("");
        TextField maxHeightDisplay = new TextField("");
        TextField finalVelDisplay = new TextField("");
        TextField totalTimeDisplay = new TextField("");
        
        Label initialVel = new Label("Initial Velocity 0m/s");
        Label angle = new Label("Take-off Angle 0\u00B0");
        Label gravity = new Label("Gravitational Field Strength 0N");
        Label earthGravity = new Label("Earth's Gravity");
        
        Slider initialVelSlider = new Slider();
        ShowTickLabels(initialVelSlider);   
        SetMidPoint(initialVelSlider, 50); 

        Slider angleSlider = new Slider();
        ShowTickLabels(angleSlider);
        SetMidPoint(angleSlider, 45);
        
        Slider gravitySlider = new Slider();
        ShowTickLabels(gravitySlider);
        SetMidPoint(gravitySlider, 25);
        
        CheckBox earthGravityBox = new CheckBox();
        
        Button fireButton = new Button("FIRE");
        ProjectileSimulatorV2.SetSizeOfButton(fireButton, 400, 100);
        

        ProjectileSimulatorV2.AddStyleToLabel(totalDisplacement, "titleLabel");
        ProjectileSimulatorV2.AddStyleToLabel(maxHeight, "titleLabel");
        ProjectileSimulatorV2.AddStyleToLabel(finalVel, "titleLabel");
        ProjectileSimulatorV2.AddStyleToLabel(totalTime, "titleLabel");
        ProjectileSimulatorV2.AddStyleToLabel(initialVel, "titleLabel");
        ProjectileSimulatorV2.AddStyleToLabel(angle, "titleLabel");
        ProjectileSimulatorV2.AddStyleToLabel(gravity, "titleLabel");
        ProjectileSimulatorV2.AddStyleToLabel(earthGravity, "titleLabel");
        
        ProjectileSimulatorV2.AddStyleToButton(fireButton);
        fireButton.setStyle("-fx-padding: 30 100 30 100;");
        
        SetSliderMinValue(initialVelSlider, 1);
        SetSliderMaxValue(initialVelSlider, 100);
        DynamicSliderReader(initialVelSlider, initialVel, "Initial Velocity: ", "m/s");
        
        SetSliderMinValue(angleSlider, 1);
        SetSliderMaxValue(angleSlider, 90);
        DynamicSliderReader(angleSlider, angle, "Take off Angle: ", "\u00B0");
      
        SetSliderMinValue(gravitySlider, 1);
        SetSliderMaxValue(gravitySlider, 50);
        DynamicSliderReader(gravitySlider, gravity, "Gravitational Field Strength: ", "N");
        
        GravityCheckboxOverridesGravitySlider(earthGravityBox, gravitySlider, gravity);
        
        DisableTypingIntoTextField(totalDisplacementDisplay);
        DisableTypingIntoTextField(maxHeightDisplay);
        DisableTypingIntoTextField(finalVelDisplay);
        DisableTypingIntoTextField(totalTimeDisplay);
        
        BorderPane container = new BorderPane();
        ProjectileSimulatorV2.SetBackgroundColour(container);
        
        VBox totalDisplacementContainer = PutDisplayBelowLabel(totalDisplacement, totalDisplacementDisplay);
        VBox maxHeightContainer = PutDisplayBelowLabel(maxHeight, maxHeightDisplay);
        VBox finalVelContainer = PutDisplayBelowLabel(finalVel, finalVelDisplay);
        VBox totalTimeContainer = PutDisplayBelowLabel(totalTime, totalTimeDisplay);
        
        HBox bottomContainer = new HBox(5);
        Region region = KeepSpacingSameInHBox();
        bottomContainer.getChildren().addAll(totalDisplacementContainer, maxHeightContainer, finalVelContainer, totalTimeContainer, region, fireButton);
        SetSpacing(bottomContainer, 30);
        
        VBox initialVelContainer = PutSliderBelowLabel(initialVel, initialVelSlider);
        VBox angleContainer = PutSliderBelowLabel(angle, angleSlider);
        VBox gravityContainer = PutSliderBelowLabel(gravity, gravitySlider);
        VBox earthGravityContainer = PutCheckBoxUnderLabel(earthGravity, earthGravityBox);
        
        VBox rightContainer = new VBox(5);
        rightContainer.getChildren().addAll(initialVelContainer, getVRegion(), angleContainer, getVRegion(), gravityContainer, getVRegion(), earthGravityContainer);
        rightContainer.setPadding(new Insets(75,50,75,50));
        
        container.setBottom(bottomContainer);
        container.setRight(rightContainer);
        LineChart graph = logic.getGraph(chartTitle);
        
        //When the button is pressed, the points are calculated, plotted and displayed on the graph
        String seriesName = " ";
        fireButton.setOnAction(e -> {
            double initialVelValue = GetSliderValue(initialVelSlider);
            double angleValue = GetSliderValue(angleSlider);
            double gravityValue = GetSliderValue(gravitySlider);
            logic.buttonAction(graph, seriesName,  initialVelValue, angleValue, gravityValue, totalDisplacementDisplay, totalTimeDisplay, maxHeightDisplay, finalVelDisplay, counter);
        });
        container.setCenter(graph);
        return container;
    }

    private double GetSliderValue(Slider slider) {
        double value = slider.getValue();
        return value;
    }

    private VBox PutCheckBoxUnderLabel(Label label, CheckBox checkBox) {
        VBox container = new VBox(5);
        container.getChildren().addAll(label,checkBox);
        return container;
    }

    private VBox PutSliderBelowLabel(Label label, Slider slider) {
        VBox container = new VBox(5);
        container.getChildren().addAll(label,slider);
        return container;
    }

    private void SetSpacing(HBox container, int spacing) {
        container.setSpacing(spacing);
        container.setPadding(new Insets(spacing,spacing,spacing,spacing));
    }

    private Region KeepSpacingSameInHBox() {
        Region region = new Region();      
        HBox.setHgrow(region, Priority.ALWAYS);
        return region;
    }

    private VBox PutDisplayBelowLabel(Label totalDisplacement, TextField totalDisplacementDisplay) {
        VBox totalDisplacementContainer = new VBox(5);
        AddLabelAndDisplayToContainer(totalDisplacementContainer,totalDisplacement, totalDisplacementDisplay);
        SetSpacingFromEdgesOfContainer(totalDisplacementContainer);
        return totalDisplacementContainer;
    }

    private void SetSpacingFromEdgesOfContainer(VBox container) {
        container.setPadding(new Insets(30,0,0,0));
    }

    private void AddLabelAndDisplayToContainer(VBox container, Label label, TextField display) {
        container.getChildren().addAll(label,display);
    }

    private void DisableTypingIntoTextField(TextField textField) {
        textField.setDisable(true);
    }

    private void GravityCheckboxOverridesGravitySlider(CheckBox earthGravityBox, Slider gravitySlider, Label gravity) {
        earthGravityBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    gravitySlider.setValue(9.81);
                    gravity.setText("Gravitational Field Strength: 10N");
                    gravitySlider.setDisable(true);
                    
                } else {
                    gravitySlider.setDisable(false);
                }
            }
        });
    }

    private void DynamicSliderReader(Slider slider, Label label, String value, String unit) {
        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                label.setText(value + (int) slider.getValue() + unit);
            }
        });
    }

    private void SetSliderMaxValue(Slider slider, int max) {
        slider.setMax(max);
    }
    
    private void SetSliderMinValue(Slider slider, int min) {
        slider.setMin(min);
    }

    private void SetMidPoint(Slider slider, int midpoint) {
        slider.setMajorTickUnit(midpoint);
    }

    private void ShowTickLabels(Slider slider) {
        slider.setShowTickLabels(true);
    }

    private String SetSimulationTitle(String title) {
        String chartTitle = title;
        return chartTitle;
    }

    public Region getVRegion() {
        Region region = new Region();
        VBox.setVgrow(region, Priority.ALWAYS);
        return region;
    }
    
    
}
