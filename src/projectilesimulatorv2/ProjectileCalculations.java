package projectilesimulatorv2;
public class ProjectileCalculations {
    public static double FindVertU(double angle, double initialVel){
        double angleRadians = Math.toRadians(angle);
        double u = initialVel*Math.sin(angleRadians);
        return u;
    }

    public static double FindHorU(double angle, double initialVel){
        double angleRadians = Math.toRadians(angle);
        double u = initialVel*Math.cos(angleRadians);
        return u;
    }

    public static double FindInitialVel(double horizontalVelocity, double verticalVelocity){
        double initialVel = Math.sqrt((horizontalVelocity * horizontalVelocity)+ (verticalVelocity * verticalVelocity));
        return initialVel;
    }
    
    public static double GetVDisplacement(double initialVel, double angle, double gravity, double elapsedTime){
        double verticalDisplacement = (elapsedTime * FindVertU(angle, initialVel)) - (0.5 * (gravity) * elapsedTime * elapsedTime);
        
        return verticalDisplacement;
    }

    public static double GetHDisplacement(double lastDisplacement, double initialVel, double angle, double elapsedTime){
        double horizontalDisplacement = (elapsedTime * FindHorU(angle, initialVel))+ lastDisplacement;
        return horizontalDisplacement;
    }

    public static double GetTotalTime(double initialVel, double angle, double gravity){
        double totalTime = FindVertU(angle, initialVel);
        totalTime /= gravity;
        totalTime *= 2;
        return totalTime;
    }

    public static double GetTotalDisplacement(double lastDisplacement, double initialVel, double angle, double gravity){
        double t = GetTotalTime(initialVel, angle, gravity);
        double totalDisplacement = (FindHorU(angle,initialVel) * t) + lastDisplacement;
        return totalDisplacement;
    }
    
    public static double GetTotalDisplacement(double lastDisplacement, double initialVel, double angle, double gravity, double ballBounciness){
        double totalDisplacement = 0;
        double vel = initialVel;
        while(GetMaxHeight(vel, angle, gravity) > 0.5){
            double newDisplacement = GetTotalDisplacement(0, vel, angle, gravity);
            totalDisplacement += newDisplacement;
            vel = ReduceInitialVel(vel, ballBounciness, angle);
        }
        return totalDisplacement;
    }

    public static double GetMaxHeight(double initialVel, double angle, double gravity){
        double maxHeight = (FindVertU(angle, initialVel) * FindVertU(angle, initialVel));
        maxHeight /=  (2 * gravity);
        return maxHeight;
    }

    public static double GetVFinalVel(double initialVel, double angle, double gravity){
        double VFinalVel = Math.sqrt(2 * gravity * GetMaxHeight(initialVel, angle, gravity));
        return VFinalVel;
    }

    public static double GetFinalVel(double initialVel, double angle, double gravity){
        double finalVel = Math.sqrt((FindHorU(initialVel, angle) * FindHorU(initialVel, angle)) + GetVFinalVel(initialVel, angle, gravity));
        return finalVel;
    }
    
    public static double ReduceInitialVel(double initialVel, double bouncinessConstant, double angle){
        double horizontalVelocity = FindHorU(angle, initialVel);
        double verticalVelocity = FindVertU(angle, initialVel);
        verticalVelocity *= bouncinessConstant;
        initialVel = FindInitialVel(horizontalVelocity, verticalVelocity);
        return initialVel;
    }
}
