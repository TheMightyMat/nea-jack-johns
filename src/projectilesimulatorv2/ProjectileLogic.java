package projectilesimulatorv2;

import javafx.animation.AnimationTimer;
import javafx.concurrent.Task;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class ProjectileLogic extends Logic{
    @Override
    public void Run(LineChart graph, XYChart.Series series, double initialVelValue, double angleValue, double gravityValue, int counter){
       plotPoints(graph, series, initialVelValue, angleValue,gravityValue, counter);
       graph.getData().add(series);
    }
    @Override
    public void plotPoints(LineChart graph, XYChart.Series series, double initialVelValue, double angleValue, double gravityValue, int counter){
        series.getData().add(new XYChart.Data(0, 0));
        long startTime = System.nanoTime();
        Task<Void> task = new Task<Void>(){
            @Override
             public Void call() throws Exception{
                new AnimationTimer() {
                @Override
                public void handle(long now) {
                    double elapsedTime = (now - startTime)/1000000000d;
                    updateGraph(graph, initialVelValue, angleValue, gravityValue, 0, ProjectileCalculations.GetTotalDisplacement(0,initialVelValue, angleValue, gravityValue),series, elapsedTime);
                if(ProjectileCalculations.GetVDisplacement(initialVelValue, angleValue, gravityValue, elapsedTime) < 0) {
                    this.stop();
                }
            }
                }.start();
                return null;
            }
        };
        new Thread(task).start();
        
    }
    
    public void setAxis(LineChart graph, double initialVelValue, double angleValue, double gravityValue){
        ((NumberAxis) graph.getYAxis()).setAutoRanging(false); // Allow manual choosing of range
        ((NumberAxis) graph.getYAxis()).setLowerBound(0); // Set start and stop of axis
        ((NumberAxis) graph.getYAxis()).setUpperBound(ProjectileCalculations.GetMaxHeight(initialVelValue, angleValue, gravityValue));
        ((NumberAxis) graph.getXAxis()).setAutoRanging(false); // Allow manual choosing of range
        ((NumberAxis) graph.getXAxis()).setLowerBound(0); // Set start and stop of axis
        ((NumberAxis) graph.getXAxis()).setUpperBound(ProjectileCalculations.GetTotalDisplacement(0, initialVelValue, angleValue, gravityValue)); 
    }
}
