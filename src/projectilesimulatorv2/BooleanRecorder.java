
package projectilesimulatorv2;

public class BooleanRecorder {
    private boolean value;
    
    public BooleanRecorder(boolean value) {
        this.value = value;
    }
    
    public void set(boolean value) {
        this.value = value;
    }
    
    public boolean get() {
        return this.value;
    }
}
